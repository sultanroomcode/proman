<?php

use yii\db\Migration;

/**
 * Class m190418_103505_projek
 */
class m190418_103505_projek extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%project_client}}', [
            'id_client' => $this->primaryKey(),
            'client_name' => $this->string()->notNull(),
            'address' => $this->string()->notNull(),

            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%project}}', [
            'id_project' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'id_client' => $this->integer(),
            
            'status' => $this->smallInteger()->notNull()->defaultValue(10),

            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey ('fk_project_client', 'project', 'id_client', 'project_client', 'id_client', 'CASCADE', 'CASCADE');

        $this->createTable('{{%project_task}}', [
            'id' => $this->bigPrimaryKey(),
            'id_project' => $this->integer(),
            'title' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),

            'status' => $this->smallInteger()->notNull()->defaultValue(0),

            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addForeignKey ('fk_project_task_link', 'project_task', 'id_project', 'project', 'id_project', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%projek}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190418_103505_projek cannot be reverted.\n";

        return false;
    }
    */
}
