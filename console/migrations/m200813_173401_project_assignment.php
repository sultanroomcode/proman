<?php

use yii\db\Migration;

/**
 * Class m200813_173401_project_assignment
 */
class m200813_173401_project_assignment extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%project_task_assignment}}', [
            'id_assignment' => $this->bigPrimaryKey(),
            'id_task' => $this->bigInteger()->notNull(),
            'id_user' => $this->integer()->notNull(),
            'optional_task' => $this->string(255)->notNull()->defaultValue('-'),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),

            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%project_task_assignment}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200813_173401_project_assignment cannot be reverted.\n";

        return false;
    }
    */
}
