<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'cdn/AdminLTE-2.3.11/dist/css/AdminLTE.min.css',
        'cdn/AdminLTE-2.3.11/dist/css/skins/_all-skins.min.css',
        'cdn/custombox/dist/custombox.min.css',
        'cdn/sweetalert2/dist/sweetalert2.min.css',
        '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css',
        '//cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css'
    ];
    public $js = [
        'cdn/bower_components/moment/min/moment.min.js',
        'cdn/bower_components/moment/locale/id.js',
        'cdn/livestamp.min.js',
        'cdn/custombox/dist/custombox.min.js',
        'cdn/AdminLTE-2.3.11/plugins/fastclick/fastclick.js',
        'cdn/AdminLTE-2.3.11/dist/js/demo.js',
        'cdn/sweetalert2/dist/sweetalert2.min.js',
        'js/app.min.js',
        'js/sts.js',
        'js/mform.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
