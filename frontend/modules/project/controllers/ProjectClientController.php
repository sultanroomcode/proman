<?php

namespace frontend\modules\project\controllers;

use Yii;
use frontend\modules\project\models\ProjectClient;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * ProjectClientController implements the CRUD actions for ProjectClient model.
 */
class ProjectClientController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                // 'only' => ['logout', 'signup'],
                'rules' => [
                    /*[
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],*/
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProjectClient models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = ProjectClient::find();

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProjectClient model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProjectClient model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProjectClient();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                $data = ['status' => 1, 'message' => 'berhasil menambah data klien',  'data' => $model];
            } else {
                $data = ['status' => 0, 'message' => 'terjadi kesalahan saat menambah data klien', 'data' => $model->getErrors()];
            }

            return $this->asJson($data);
        } else {

            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ProjectClient model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                $data = ['status' => 1, 'message' => 'berhasil update data klien',  'data' => $model];
            } else {
                $data = ['status' => 0, 'message' => 'terjadi kesalahan saat update data klien', 'data' => $model->getErrors()];
            }

            return $this->asJson($data);
        } else {

            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ProjectClient model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->delete()) {
            $data = ['status' => 1, 'message' => 'Berhasil menghapus data klien',  'data' => $model];
        } else {
            $data = ['status' => 0, 'message' => 'terjadi kesalahan saat menghapus data klien', 'data' => $model->getErrors()];
        }

        return $this->asJson($data);
    }

    /**
     * Finds the ProjectClient model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProjectClient the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProjectClient::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
