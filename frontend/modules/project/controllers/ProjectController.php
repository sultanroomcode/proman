<?php

namespace frontend\modules\project\controllers;

use Yii;
use frontend\modules\project\models\Project;
use frontend\modules\project\models\ProjectTask;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * ProjectController implements the CRUD actions for Project model.
 */
class ProjectController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                // 'only' => ['logout', 'signup'],
                'rules' => [
                    /*[
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],*/
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = Project::find();

        return $this->renderAjax('index', [
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ProjectTask::find()->orderBy(['created_at' => SORT_DESC])->where(['id_project' => $id]),
        ]);

        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id = null)
    {
        $model = new Project();

        if ($id != null) {
            $model->id_client = $id;
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                $data = ['status' => 1, 'message' => 'berhasil menambah data projek',  'data' => $model];
            } else {
                $data = ['status' => 0, 'message' => 'terjadi kesalahan saat menambah data projek', 'data' => $model->getErrors()];
            }

            return $this->asJson($data);
        } else {

            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                $data = ['status' => 1, 'message' => 'berhasil menambah data projek',  'data' => $model];
            } else {
                $data = ['status' => 0, 'message' => 'terjadi kesalahan saat menambah data projek', 'data' => $model->getErrors()];
            }

            return $this->asJson($data);
        } else {

            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->delete()) {
            $data = ['status' => 1, 'message' => 'Berhasil menghapus data projek',  'data' => $model];
        } else {
            $data = ['status' => 0, 'message' => 'terjadi kesalahan saat menghapus data projek', 'data' => $model->getErrors()];
        }

        return $this->asJson($data);
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
