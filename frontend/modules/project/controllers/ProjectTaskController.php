<?php

namespace frontend\modules\project\controllers;

use Yii;
use frontend\modules\project\models\ProjectTask;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * ProjectTaskController implements the CRUD actions for ProjectTask model.
 */
class ProjectTaskController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                // 'only' => ['logout', 'signup'],
                'rules' => [
                    /*[
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],*/
                    [
                        'actions' => ['index', 'history', 'list', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProjectTask models.
     * @return mixed
     */
    public function actionHistory()
    {
        $model = ProjectTask::find();
        $model->orderBy('updated_at DESC');

        return $this->renderAjax('history', [
            'model' => $model
        ]);
    }

    public function actionList($id)
    {
        $model = ProjectTask::find();
        $model->where(['id_project' => $id, 'root_id' => 0]);

        return $this->renderAjax('list', [
            'model' => $model,
            'id' => $id,
        ]);
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ProjectTask::find(),
        ]);

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProjectTask model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProjectTask model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id = null)
    {
        $model = new ProjectTask();

        if ($id != null) {
            $model->id_project = $id;
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                $data = ['status' => 1, 'message' => 'berhasil menambah data task projek',  'data' => $model];
            } else {
                $data = ['status' => 0, 'message' => 'terjadi kesalahan saat menambah data task projek', 'data' => $model->getErrors()];
            }

            return $this->asJson($data);
        } else {

            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ProjectTask model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $id_project = null)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                $data = ['status' => 1, 'message' => 'berhasil update data task',  'data' => $model];
            } else {
                $data = ['status' => 0, 'message' => 'terjadi kesalahan saat update data task', 'data' => $model->getErrors()];
            }

            return $this->asJson($data);
        } else {

            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ProjectTask model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $id_project = null)
    {
        $model = $this->findModel($id);
        if ($model->delete()) {
            $data = ['status' => 1, 'message' => 'Berhasil menghapus data task',  'data' => $model];
        } else {
            $data = ['status' => 0, 'message' => 'terjadi kesalahan saat menghapus data task', 'data' => $model->getErrors()];
        }

        return $this->asJson($data);
    }

    /**
     * Finds the ProjectTask model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProjectTask the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProjectTask::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
