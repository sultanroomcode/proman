<?php

namespace frontend\modules\project\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use common\models\User;

/**
 * This is the model class for table "project".
 *
 * @property int $id_project
 * @property string $title
 * @property int $id_client
 * @property int $status
 * @property int $created_by
 * @property int $created_at
 * @property int $updated_at
 *
 * @property ProjectClient $client
 * @property ProjectTask[] $projectTasks
 */
class Project extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $user;
    public static function tableName()
    {
        return 'project';
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // 'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['id_client', 'status'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['id_client'], 'exist', 'skipOnError' => true, 'targetClass' => ProjectClient::class, 'targetAttribute' => ['id_client' => 'id_client']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_project' => 'ID Projek',
            'title' => 'Nama Projek',
            'id_client' => 'Klien',
            'status' => 'Status',

            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getUsercreator() //c/u
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    public function getUserupdater() //c/u
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }

    public function getClient()
    {
        return $this->hasOne(ProjectClient::class, ['id_client' => 'id_client']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(ProjectTask::class, ['id_project' => 'id_project']);
    }

    public function getTasksunstart()
    {
        return $this->hasMany(ProjectTask::class, ['id_project' => 'id_project'])->onCondition(['status' => 0]);
    }

    public function getTasksfinished()
    {
        return $this->hasMany(ProjectTask::class, ['id_project' => 'id_project'])->onCondition(['status' => 10]);
    }

    public function getTasksprogress()
    {
        return $this->hasMany(ProjectTask::class, ['id_project' => 'id_project'])->onCondition(['status' => 5]);
    }
}
