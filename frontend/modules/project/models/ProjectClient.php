<?php

namespace frontend\modules\project\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use common\models\User;

/**
 * This is the model class for table "project_client".
 *
 * @property int $id_client
 * @property string $client_name
 * @property string $address
 * @property int $created_by
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Project[] $projects
 */
class ProjectClient extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'project_client';
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // 'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_name', 'address'], 'required'],
            [['client_name', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_client' => 'Id Client',
            'client_name' => 'Client Name',
            'address' => 'Address',

            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::class, ['id_client' => 'id_client']);
    }

    public function getUsercreator() //c/u
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    public function getUserupdater() //c/u
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }
}
