<?php

namespace frontend\modules\project\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use common\models\User;

/**
 * This is the model class for table "project_task".
 *
 * @property int $id
 * @property int $id_project
 * @property int $ord_id
 * @property int $root_id
 * @property string $title
 * @property string $description
 * @property int $status
 * @property int $created_by
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Project $project
 */
class ProjectTask extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'project_task';
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // 'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_project', 'status', 'ord_id', 'root_id'], 'integer'],
            [['title', 'description'], 'required'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['id_project'], 'exist', 'skipOnError' => true, 'targetClass' => Project::class, 'targetAttribute' => ['id_project' => 'id_project']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_project' => 'Id Project',
            'root_id' => 'ID Akar',
            'title' => 'Judul',
            'description' => 'Konten',
            'status' => 'Status',

            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::class, ['id_project' => 'id_project']);
    }

    public function humanStatus()
    {
        if ($this->status == 0) {
            return '<span class="label label-info">Rencana</span>';
        } else if ($this->status == 5) {
            return '<span class="label label-warning">Sedang Dikerjakan</span>';
        } else if ($this->status == 10) {
            return '<span class="label label-success">Sudah Dikerjakan</span>';
        }
    }

    public function getUsercreator() //c/u
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    public function getUserupdater() //c/u
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }

    public function getChildtask() //c/u
    {
        return $this->hasMany(ProjectTask::class, ['root_id' => 'id']);
    }

    public function genNum() //generate number
    {
        //look for the max of
        $mx = $this->find()->where(['id_project' => $this->id_project])->max('ord_id');
        if ($mx == null) {
            $this->ord_id = 1;
        } else {
            $this->ord_id = $mx + 1;
        }
    }
}
