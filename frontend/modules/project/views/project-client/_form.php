<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\project\models\ProjectClient */
/* @var $form yii\widgets\ActiveForm */

$arrFormConfig = [
    'id' => $model->formName()
];

$backUrl = 'goLoad({url:\'/project/project-client\'});';
?>

<div class="project-client-form">
    <div class="panel panel-default">
        <div class="panel-body">
            <?php $form = ActiveForm::begin($arrFormConfig); ?>

            <?= $form->field($model, 'client_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton('Simpan', ['id' => 'btn-submit-client', 'class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<?php
$script = <<<JS
//regularly-ajax
sendForm({formName: 'form#{$model->formName()}', module:'project > client > form',disabledButton:'#btn-submit-client', successTask:function(res){
  console.log(res);
  responseModal({
    title:'Berhasil',
    type:'success',
    target:'body',
    text:res.message,
    timer:3000,
    onAfterClose:function(){
        {$backUrl}
    }
  });

}});
JS;

$this->registerJs($script);
