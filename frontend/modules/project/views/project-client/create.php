<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\project\models\ProjectClient */

$this->title = 'Tambah Klien';
$this->params['breadcrumbs'][] = ['label' => 'Project Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-client-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>