<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Klien';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-client-index">
    <div class="panel panel-default">
        <div class="panel-body">


            <h1><?= Html::encode($this->title) ?></h1>

            <p>
                <a href="javascript:void(0)" onclick="goLoad({url:'/project/project-client/create'})" class="btn btn-primary btn-sm"> Tambahkan Klien </a>
            </p>
        </div>

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Jumlah Projek</th>
                    <th>Pembuat Klien</th>
                    <th>Dibuat</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <?php foreach ($dataProvider->all() as $v) : ?>
                <tr>
                    <td><?= $v->id_client ?></td>
                    <td><?= $v->client_name ?></td>
                    <td><?= $v->address ?></td>
                    <td><?= count($v->projects) ?></td>
                    <td><?= $v->usercreator->username ?></td>
                    <td><?= $v->created_at ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="goLoad({url:'/project/project-client/view?id=<?= $v->id_client ?>'})" class="btn btn-sm btn-success"> lihat </a>
                        <a href="javascript:void(0)" onclick="goLoad({url:'/project/project-client/update?id=<?= $v->id_client ?>'})" class="btn btn-sm btn-warning"> ubah </a>
                        <a href="javascript:void(0)" onclick="goLoad({url:'url'})" class="btn btn-sm btn-danger"> hapus </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

        <div class="panel-footer">
            <ul class="pagination pull-right">
                <li>
                    <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li>
                    <a href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
    </div>
</div>