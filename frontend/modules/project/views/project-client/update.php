<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\project\models\ProjectClient */

$this->title = 'Update Klien : ' . $model->client_name;
$this->params['breadcrumbs'][] = ['label' => 'Project Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_client, 'url' => ['view', 'id' => $model->id_client]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="project-client-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>