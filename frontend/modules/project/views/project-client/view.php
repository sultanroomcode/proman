<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\project\models\ProjectClient */

$this->title = $model->client_name;
$this->params['breadcrumbs'][] = ['label' => 'Project Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="project-client-view">
    <div class="panel panel-default">
        <div class="panel-body">
            <h1><?= Html::encode($this->title) ?></h1>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id_client',
                    'client_name',
                    'address',
                    'created_by',
                    'created_at',
                    'updated_at',
                ],
            ]) ?>

            <a href="javascript:void(0)" onclick="goLoad({url:'/project/project/create?id=<?= $model->id_client ?>'})" class="btn btn-primary btn-sm"> Tambahkan Projek </a>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Projek</th>
                    </tr>
                </thead>

                <tbody>
                    <?php foreach ($model->projects as $project) : ?>
                        <tr>
                            <td><?= $project->title ?></td>
                            <td><?= $project->status ?></td>
                            <td>
                                <a href="javascript:void(0)" onclick="goLoad({url:'/project/project/view?id=<?= $project->id_project ?>'})" class="btn btn-primary btn-sm"> Lihat Projek </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>