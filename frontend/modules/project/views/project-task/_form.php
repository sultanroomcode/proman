<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use frontend\modules\project\models\Project;
use frontend\modules\project\models\ProjectTask;

/* @var $this yii\web\View */
/* @var $model frontend\modules\project\models\ProjectTask */
/* @var $form yii\widgets\ActiveForm */

$arr = ArrayHelper::map(Project::find()->all(), 'id_project', 'title');
if ($model->isNewRecord) {
    $model->genNum();
}

$arrFormConfig = [
    'id' => $model->formName()
];

$backUrl = 'goLoad({elm:\'#task-panel-area\', url:\'/project/project-task/list?id=' . $model->id_project . '\'});';
$arrParent = ArrayHelper::map(ProjectTask::find()->where(['id_project' =>  $model->id_project])->all(), 'id', 'title');
$arrParent = ArrayHelper::merge([0 => 'Tugas Utama'], $arrParent);
?>

<div class="project-task-form">

    <?php $form = ActiveForm::begin($arrFormConfig); ?>

    <?= $form->field($model, 'id_project')->dropdownList($arr) ?>

    <?= $form->field($model, 'ord_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'root_id')->dropdownList($arrParent) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>


    <?= $form->field($model, 'status')->dropdownList([0 => 'Requested', 5 => 'Work on It', 10 => 'Finish']) ?>

    <div class="form-group">
        <?= Html::submitButton('Simpan', ['id' => 'btn-submit-client', 'class' => 'btn btn-success']) ?>
        <a href="javascript:void(0)" onclick="goLoad({elm:'#task-panel-area', url:'/project/project-task/list?id=<?= $model->id_project ?>'})" class="btn btn-primary"> Kembali </a>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = <<<JS
//regularly-ajax
sendForm({formName: 'form#{$model->formName()}', module:'project > task > form',disabledButton:'#btn-submit-client', successTask:function(res){
  console.log(res);
  responseModal({
    title:'Berhasil',
    type:'success',
    target:'body',
    text:res.message,
    timer:3000,
    onAfterClose:function(){
        {$backUrl}
    }
  });

}});
JS;

$this->registerJs($script);
