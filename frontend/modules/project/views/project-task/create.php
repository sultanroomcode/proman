<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\project\models\ProjectTask */

$this->title = 'Buat Tugas Projek Baru';
$this->params['breadcrumbs'][] = ['label' => 'Project Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-task-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>