<ul class="timeline">
    <?php foreach ($model->all() as $v) : ?>
        <li>
            <i class="fa fa-envelope bg-blue"></i>

            <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> <?= '<span data-livestamp="' . $v->created_at . '"></span>' ?> <i class="fa fa-clock-o"></i> <?= '<span data-livestamp="' . $v->updated_at . '"></span>' ?></span>

                <h3 class="timeline-header"><?= $v->title ?></h3>

                <div class="timeline-body">
                    <?= '<p class="breakword"><b>' . $v->title . '</b><br>' . html_entity_decode($v->description) . '<br><span class="label label-info"><i class="fa fa-user"></i> ' . $v->usercreator->username . '</span><input class="form-control" onClick="this.select();" type="text" value="' . $v->title . '#' . $v->ord_id . '"></p>' ?>
                </div>
                <div class="timeline-footer">
                    <a href="javascript:void(0)" onclick="goLoad({url:'/project/project/view?id=<?= $v->id_project ?>'})" class="btn btn-primary btn-xs">Project</a>
                </div>
            </div>
        </li>
    <?php endforeach; ?>
    <li>
        <i class="fa fa-clock-o bg-gray"></i>
    </li>
</ul>