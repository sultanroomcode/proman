<a href="javascript:void(0)" onclick="goLoad({elm:'#task-panel-area',url:'/project/project-task/create?id=<?= $id ?>'})" class="btn btn-primary btn-sm"> Tambahkan Task </a>

<table class="table">
    <thead>
        <tr>
            <th>ID</th>
            <th>Urutan</th>
            <th>Status</th>
            <th>Utilitas</th>
            <th>Dibuat</th>
            <th>Diupdate</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($model->all() as $v) : ?>
            <tr>
                <td><?= $v->id ?></td>
                <td><?= $v->ord_id ?></td>
                <td><?= '<p class="breakword"><b>' . $v->title . '</b><br>' . html_entity_decode($v->description) . '<br><span class="label label-info"><i class="fa fa-user"></i> ' . $v->usercreator->username . '</span><input class="form-control" id="task-' . $v->id . '" onClick="myFunction(this.id)" type="text" value="' . $v->title . '#' . $v->ord_id . '"></p>' ?>
                    <hr>
                    <ul>
                        <?php foreach ($v->childtask as $c) : ?>
                            <li><?= $c->title ?> - <?= $c->humanStatus() ?> - <?= $c->description ?> - <?= '<span data-livestamp="' . $c->created_at . '"></span>' ?> <a href="javascript:void(0)" onclick="goLoad({elm:'#task-panel-area', url:'/project/project-task/update?id=<?= $c->id ?>'})" class="label label-warning"> ubah </a></li>
                        <?php endforeach; ?>
                    </ul>

                </td>
                <td><?= $v->humanStatus() ?></td>
                <td><?= '<span data-livestamp="' . $v->created_at . '"></span>' ?></td>
                <td><?= '<span data-livestamp="' . $v->updated_at . '"></span>' ?></td>

                <td>
                    <a href="javascript:void(0)" onclick="goLoad({elm:'#task-panel-area', url:'/project/project-task/update?id=<?= $v->id ?>'})" class="btn btn-sm btn-warning"> ubah </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$script = <<<JS
function myFunction(id) {
  var copyText = document.getElementById(id);
  copyText.select();
  copyText.setSelectionRange(0, 99999)
  document.execCommand("copy");
//   alert("Copied the text: " + copyText.value);
}
JS;

$this->registerJs($script);
