<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use frontend\modules\project\models\ProjectClient;
/* @var $this yii\web\View */
/* @var $model frontend\modules\project\models\Project */
/* @var $form yii\widgets\ActiveForm */

$arr = ArrayHelper::map(ProjectClient::find()->all(), 'id_client', 'client_name');

$arrFormConfig = [
    'id' => $model->formName()
];

$backUrl = 'goLoad({url:\'/project/project-client/view?id=' . $model->id_client . '\'});';
?>

<div class="project-form">
    <div class="panel panel-default">
        <div class="panel-body">
            <?php $form = ActiveForm::begin($arrFormConfig); ?>

            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'id_client')->dropdownList($arr) ?>

            <?= $form->field($model, 'status')->dropdownList([0 => 'Non-Aktif', 10 => 'Aktif']) ?>

            <div class="form-group">
                <?= Html::submitButton('Simpan', ['id' => 'btn-submit-client', 'class' => 'btn btn-success']) ?>
                <a href="javascript:void(0)" onclick="<?= $backUrl ?>" class="btn btn-primary"> Kembali </a>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<?php
$script = <<<JS
//regularly-ajax
sendForm({formName: 'form#{$model->formName()}', module:'project > project > form',disabledButton:'#btn-submit-client', successTask:function(res){
  console.log(res);
  responseModal({
    title:'Berhasil',
    type:'success',
    target:'body',
    text:res.message,
    timer:3000,
    onAfterClose:function(){
        {$backUrl}
    }
  });

}});
JS;

$this->registerJs($script);
