<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model frontend\modules\project\models\Project */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<style type="text/css">
    .breakword {
        display: inline-block;
        width: 700px;
        word-wrap: break-word;
        white-space: pre-wrap;
        hyphens: auto;
    }
</style>
<div class="project-view">
    <div class="panel panel-default">
        <div class="panel-body">
            <h1><?= Html::encode($this->title) ?></h1>

            <table class="table table-striped table-bordered detail-view">
                <tr>
                    <th width="200">ID Projek</th>
                    <td><?= $model->id_project ?></td>
                </tr>

                <tr>
                    <th>Nama Projek</th>
                    <td><?= $model->title ?></td>
                </tr>

                <tr>
                    <th>Klien</th>
                    <td><?= $model->client->client_name ?></td>
                </tr>

                <tr>
                    <th>Status</th>
                    <td><?= $model->status ?></td>
                </tr>

                <tr>
                    <th>Pembuat</th>
                    <td><?= $model->usercreator->username ?></td>
                </tr>

                <tr>
                    <th>Dibuat pada tanggal</th>
                    <td><?= date('d-F-Y H:i', $model->created_at) ?></td>
                </tr>
            </table>

            <a href="javascript:void(0)" onclick="goLoad({url:'/project/project-task/create?id=<?= $model->id_project ?>'})" class="btn btn-primary btn-sm"> Tambahkan Task </a>

            <div class="table-responsive">
                <?php Pjax::begin() ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        // 'id',
                        // 'id_project',
                        'ord_id',
                        // 'description:ntext'
                        [
                            'attribute' => 'title',
                            'label' => 'Task',
                            'options' => ['class' => 'breakword'],
                            // 'contentOptions' =>function ($model, $key, $index, $column){
                            //     return ['class' => 'tbl_column_name'];
                            // },
                            'content' => function ($m) {
                                return '<p class="breakword"><b>' . $m->title . '</b><br>' . html_entity_decode($m->description) . '<br><span class="label label-info"><i class="fa fa-user"></i> ' . $m->usercreator->username . '</span><input class="form-control" onClick="this.select();" type="text" value="' . $m->title . '#' . $m->ord_id . '"></p>';
                            }
                        ],
                        [
                            'attribute' => 'status',
                            'label' => 'Status',
                            // 'contentOptions' =>function ($model, $key, $index, $column){
                            //     return ['class' => 'tbl_column_name'];
                            // },
                            'content' => function ($m) {
                                return $m->humanStatus();
                            }
                        ],
                        [
                            'attribute' => 'created_at',
                            'label' => 'Tanggal Masuk',
                            // 'contentOptions' =>function ($model, $key, $index, $column){
                            //     return ['class' => 'tbl_column_name'];
                            // },
                            'content' => function ($m) {
                                return '<span data-livestamp="' . $m->created_at . '"></span>';
                            }
                        ],
                        [
                            'attribute' => 'updated_at',
                            'label' => 'Tanggal Update',
                            // 'contentOptions' =>function ($model, $key, $index, $column){
                            //     return ['class' => 'tbl_column_name'];
                            // },
                            'content' => function ($m) {
                                // return date('d-F-Y H:i', $m->updated_at);
                                return '<span data-livestamp="' . $m->updated_at . '"></span>';
                            }
                        ],
                        //'created_by',
                        //'created_at',
                        //'updated_at',

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{popup} {view}<br>{update} {delete}',
                            'urlCreator' => function ($action, $model, $key, $index) {
                                $url_base = Url::base();
                                if ($action === 'view') {
                                    $url = $url_base . '/project/project-task/view?id=' . $model->id;
                                    return $url;
                                }

                                if ($action === 'update') {
                                    $url = $url_base . '/project/project-task/update?id=' . $model->id . '&id_project=' . $model->id_project;
                                    return $url;
                                }
                                if ($action === 'delete') {
                                    $url = $url_base . '/project/project-task/delete?id=' . $model->id . '&id_project=' . $model->id_project;
                                    return $url;
                                }
                            },
                            'buttons' => [
                                'edit' => function ($url, $model) {
                                    return Html::a(
                                        '<span class="glyphicon glyphicon-new-window"></span> ',
                                        $url,
                                        [
                                            'title' => 'Edit ',
                                            'data-pjax' => '0',
                                        ]
                                    );
                                },
                                'popup' => function ($url, $model) {
                                    $urledited = html_entity_decode("url:'" . substr($url, strrpos($url, '?')) . "'");
                                    return Html::a(
                                        '<span class="glyphicon glyphicon-new-window"></span> ',
                                        'javascript:void(0)',
                                        [
                                            'onclick' => "goPopup({" . $urledited . "})",
                                            'title' => 'Buka popup Edit ',
                                            'data-pjax' => '0',
                                        ]
                                    );
                                }
                            ],
                        ],
                    ],
                ]); ?>

                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>