<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projek';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">
    <div class="panel panel-default">
        <div class="panel-body">
            <h1><?= Html::encode($this->title) ?></h1>

            <p>
                <a href="javascript:void(0)" onclick="goLoad({url:'/project/project/create'})" class="btn btn-primary btn-sm"> Tambahkan Projek </a>
            </p>

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th rowspan="2">ID</th>
                        <th rowspan="2">Nama</th>
                        <th rowspan="2">Klien</th>
                        <th colspan="4">Jumlah Tugas</th>
                        <th rowspan="2">Status</th>
                        <th rowspan="2">Aksi</th>
                    </tr>

                    <tr>
                        <th>Unstart</th>
                        <th>Progress</th>
                        <th>Finish</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <?php foreach ($model->all() as $v) : ?>
                    <tr>
                        <td><?= $v->id_project ?></td>
                        <td><?= $v->title ?></td>
                        <td><?= $v->client->client_name ?></td>
                        <td><?= count($v->tasksunstart) ?></td>
                        <td><?= count($v->tasksprogress) ?></td>
                        <td><?= count($v->tasksfinished) ?></td>
                        <td><?= count($v->tasks) ?></td>
                        <td><?= $v->status ?></td>
                        <td>
                            <a href="javascript:void(0)" onclick="goLoad({url:'/project/project/view?id=<?= $v->id_project ?>'})" class="btn btn-sm btn-success"> lihat </a>
                            <a href="javascript:void(0)" onclick="goLoad({url:'/project/project/update?id=<?= $v->id_project ?>'})" class="btn btn-sm btn-warning"> ubah </a>
                            <a href="javascript:void(0)" onclick="goLoad({url:'url'})" class="btn btn-sm btn-danger"> hapus </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>