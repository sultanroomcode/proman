<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model frontend\modules\project\models\Project */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<style type="text/css">
    .breakword {
        display: inline-block;
        width: 700px;
        word-wrap: break-word;
        white-space: pre-wrap;
        hyphens: auto;
    }
</style>
<div class="project-view">
    <div class="panel panel-default">
        <div class="panel-body">
            <h1><?= Html::encode($this->title) ?></h1>

            <table class="table table-striped table-bordered detail-view">
                <tr>
                    <th width="200">ID Projek</th>
                    <td><?= $model->id_project ?></td>
                </tr>

                <tr>
                    <th>Nama Projek</th>
                    <td><?= $model->title ?></td>
                </tr>

                <tr>
                    <th>Klien</th>
                    <td><?= $model->client->client_name ?></td>
                </tr>

                <tr>
                    <th>Status</th>
                    <td><?= $model->status ?></td>
                </tr>

                <tr>
                    <th>Pembuat</th>
                    <td><?= $model->usercreator->username ?></td>
                </tr>

                <tr>
                    <th>Dibuat pada tanggal</th>
                    <td><?= date('d-F-Y H:i', $model->created_at) ?></td>
                </tr>
            </table>



            <div class="table-responsive">
                <div id="task-panel-area"></div>
            </div>
        </div>
    </div>
</div>
<?php
$script = <<<JS
//regularly-ajax
goLoad({elm:'#task-panel-area', url:'/project/project-task/list?id={$model->id_project}'})
JS;

$this->registerJs($script);
