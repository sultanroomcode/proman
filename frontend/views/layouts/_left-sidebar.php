<?php

use yii\helpers\Url;
use yii\helpers\Html;
?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <?php if (!Yii::$app->user->isGuest) : ?>
          <p><?= Yii::$app->user->identity->username ?></p>
          <a href="<?= Url::to(['/']) ?>"><i class="fa fa-circle text-success"></i> Online</a>
        <?php endif; ?>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Cari...">
        <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
          </button>
        </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">NAVIGASI UTAMA</li>
      <li class="active treeview">
        <a href="<?= Url::to(['/']) ?>">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="javascript:void(0)" onclick="goLoad({url:'/project/project-client'})"><i class="fa fa-circle-o"></i> Client</a></li>
          <li><a href="javascript:void(0)" onclick="goLoad({url:'/project/project'})"><i class="fa fa-circle-o"></i> Projek</a></li>
        </ul>
      </li>
      <?php if (!Yii::$app->user->isGuest) : ?>
        <li class="treeview">
          <a href="<?= Url::to(['/']) ?>">
            <i class="fa fa-check"></i> <span>Tugas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="javascript:void(0)" onclick="goLoad({url:'/project/project-task/history'})"><i class="fa fa-circle-o"></i> Histori Tugas</a></li>
            <li><a href="javascript:void(0)" onclick="goLoad({url:'/project/project-client'})"><i class="fa fa-circle-o"></i> Klien</a></li>
            <li><a href="javascript:void(0)" onclick="goLoad({url:'/project/project'})"><i class="fa fa-circle-o"></i> Proyek</a></li>
            <li><a href="javascript:void(0)" onclick="goLoad({url:'/project/project'})"><i class="fa fa-circle-o"></i> Pengingat</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="<?= Url::to(['/']) ?>">
            <i class="fa fa-check"></i> <span>Catatan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="javascript:void(0)" onclick="goLoad({url:'/notes/blog-notes/list'})"><i class="fa fa-circle-o"></i> Daftar Catatan</a></li>
          </ul>
        </li>
        <li><a href="javascript:void(0)"><i class="fa sign-out"></i>
            <?= Html::beginForm(['/site/logout'], 'post')
              . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-danger']
              )
              . Html::endForm(); ?>
          </a>
        </li>
      <?php endif; ?>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>