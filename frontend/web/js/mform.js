// elm1, elm2, type
function getUsermanOptionForm(o) {
	var kode = o.elm1;
	$(o.elm2).html('<option value=""></option>');
	if (kode !== '') {
		goLoad({
			elm: o.elm2,
			url: '/userman/user-dashboard/tool-data-region-option?type=' + o.type + '&kode=' + kode
		});
	}
}

function sendFormData(o) {
	/*
	requirement : need place on modal based form
	att :
	formName
	modulname
	disabledButton
	successTask (optional)
	*/
	if (o.hasOwnProperty('xhrActivated')) {
		$(o.xhrActivated).hide();
	}

	let f = $(o.formName).on('beforeSubmit', function (e) {
		var $form = $(this);
		var $data = new FormData($(o.formName)[0]);

		let xhrRequest = () => {};
		let xhrBeforeRequest = () => {};


		if (o.hasOwnProperty('xhrActivated')) {
			$(o.xhrActivated).hide();
			xhrRequest = () => {
				var xhr = new window.XMLHttpRequest();
				xhr.upload.addEventListener("progress", function (evt) {
					if (evt.lengthComputable) {
						var percentComplete = ((evt.loaded / evt.total) * 100);
						$(o.xhrActivated + ' > span').width(percentComplete + '%');
					}
				}, false);
				return xhr;
			};
			xhrBeforeRequest = () => {
				$(o.xhrActivated).show();
				$(o.xhrActivated + ' > span').width('0%');

				console.log('xhr progressbar start');
			};

			console.log('xhr progressbar activated');
		}

		if (o.hasOwnProperty('disabledButton')) {
			$(o.disabledButton).hide();
			$(o.disabledButton).attr('disabled', true);
			$(o.disabledButton).after('<div id="loaderspin-x"><img class="loaderspin"></></div>');
			// $(o.disabledButton).prepend('<img id="loaderspin-x" class="loaderspin"></>');
			console.log('start');
		}

		$.ajax({
			url: $form.attr('action'),
			data: $data,
			beforeSend: xhrBeforeRequest,
			xhr: xhrRequest,
			type: 'post',
			processData: false,
			contentType: false,
			success: function (resJson) {
				if (resJson.status == 1) {
					Toast.fire({
						title: resJson.message,
						type: 'success',
						target: '#main-modal'
					});
					$($form).trigger('reset');
					if (o.hasOwnProperty('successTask')) {
						o.successTask(resJson);
					}
				} else {
					var returntext = '';
					$.each(resJson.data, function (i, val) {
						$.each(val, function (il, v) {
							returntext += v + '<br>';
						});
					});
					Toast.fire({
						title: 'Validasi Gagal',
						type: 'error',
						timer: 6000,
						text: resJson.message + '\n' + returntext,
						target: '#main-modal'
					});
				}

				if (o.hasOwnProperty('xhrActivated')) {
					$(o.xhrActivated).hide();
					console.log('xhr progressbar stop');
				}

				if (o.hasOwnProperty('disabledButton')) {
					// $(o.disabledButton).attr('disabled', false);
					$('#loaderspin-x').remove();
					// $(o.disabledButton).attr('disabled', true);
					$(o.disabledButton).show();
					console.log('stop');
				}
			},
			error: responseCall
		});

		return false;
	});

	return f;
}

function sendForm(o) {
	// $('form#'+o.formName).on('beforeSubmit', function(e){
	var f = $(o.formName).on('beforeSubmit', function (e) {
		var $form = $(this);
		if (o.hasOwnProperty('disabledButton')) {
			$(o.disabledButton).hide();
			$(o.disabledButton).attr('disabled', true);
			$(o.disabledButton).after('<div id="loaderspin-x"><img class="loaderspin"></></div>');
			// $(o.disabledButton).prepend('<img id="loaderspin-x" class="loaderspin"></>');
			console.log('start');
		}

		$.post(
			$form.attr('action'),
			$form.serialize()
		).done(function (resJson) {
			if (resJson.status == 1) {
				Toast.fire({
					title: resJson.message,
					type: 'success',
					target: '#main-modal'
				});
				$($form).trigger('reset');
				if (o.hasOwnProperty('successTask')) {
					o.successTask(resJson);
				}
			} else {
				var returntext = '';
				$.each(resJson.data, function (i, val) {
					$.each(val, function (il, v) {
						returntext += v + '<br>';
					});
				});
				Toast.fire({
					title: 'Validasi Gagal',
					type: 'error',
					timer: 6000,
					text: resJson.message + '\n' + returntext,
					target: 'body'
				});
			}

			if (o.hasOwnProperty('disabledButton')) {
				// $(o.disabledButton).attr('disabled', false);
				$('#loaderspin-x').remove();
				// $(o.disabledButton).attr('disabled', true);
				$(o.disabledButton).show();
				console.log('stop');
			}
		}).fail(function () {
			Toast.fire({
				title: 'Server sedang mengalami proses sibuk/bermasalah! hubungi admin untuk perbaikan modul #' + o.modulname,
				type: 'error',
				target: '#main-modal'
			});
		});
		e.preventDefault();
		return false;
	});

	return f;
}

function sendFormOnPopup(o) {
	/*
	requirement : need place on modal based form
	att :
	formName
	modulname
	target
	successTask (optional)
	*/
	let targetDiv = (o.hasOwnProperty('target')) ? o.target : '#main-modal';
	let f = $(o.formName).on('beforeSubmit', function (e) {
		var $form = $(this);
		$.post(
			$form.attr('action'),
			$form.serialize()
		).done(function (resJson) {
			if (o.hasOwnProperty('successTask')) {
				o.successTask(resJson, $form);
			}
		}).fail(function () {
			Toast.fire({
				title: 'Server sedang mengalami proses sibuk/bermasalah! hubungi admin untuk perbaikan modul #' + o.modulname,
				type: 'error',
				target: targetDiv
			});
		});
		e.preventDefault();
		return false;
	});

	return f;
}

//for upload
function sendFormUpload(o) {
	// $('form#'+o.formName).on('beforeSubmit', function(e){
	let f = $(o.formName).ajaxForm({
		beforeSubmit: function () {

		},
		beforeSend: function () {
			var percentVal = 0;
			bar.attr('aria-valuenow', percentVal);
			bar.css({
				'width': '0%'
			});
			percent.html(percentVal + ' %');
		},
		uploadProgress: function (event, position, total, percentComplete) {
			var percentVal = percentComplete;
			bar.attr('aria-valuenow', percentVal);
			bar.css({
				'width': percentVal + '%'
			});
			percent.html(percentVal + ' %');
		},
		success: function () {
			var percentVal = 100;
			bar.attr('aria-valuenow', percentVal);
			bar.css({
				'width': percentVal + '%'
			});
			percent.html(percentVal + ' %');
		},
		complete: function (xhr) {
			if (o.hasOwnProperty('successTask')) {
				o.successTask(xhr);
			}
		}
	});

	return f;
}