var base_url = $("body").attr("data-url");
var main_el = $("div#main-container");
var elementAccess = null;
main_el.text("Project Manager Sultan Inv");

function goLoad(o) {
  var dataObject = null;
  if (o.hasOwnProperty("dataObject")) {
    dataObject = JSON.stringify(o.dataObject);
  }

  if (o.hasOwnProperty("elm")) {
    elementAccess = o.elm;
    if (dataObject !== null) {
      $(o.elm).load(
        base_url + o.url,
        {
          data: dataObject,
        },
        responseCall
      );
    } else {
      $(o.elm).load(base_url + o.url, responseCall);
    }
  } else {
    if (dataObject !== null) {
      main_el.load(
        base_url + o.url,
        {
          data: dataObject,
        },
        responseCall
      );
    } else {
      main_el.load(base_url + o.url, responseCall);
    }
  }
}

/*
when elm1, is probably not exist than use elm2
*/
function goLoadAlt(o) {
  var dataObject = null;
  if (o.hasOwnProperty("dataObject")) {
    dataObject = JSON.stringify(o.dataObject);
  }

  if (o.hasOwnProperty("elm")) {
    elementAccess = o.elm;
    if ($(o.elm).length) {
      if (dataObject !== null) {
        $(o.elm).load(
          base_url + o.url,
          {
            data: dataObject,
          },
          responseCall
        );
      } else {
        $(o.elm).load(base_url + o.url, responseCall);
      }
    } else {
      elementAccess = o.elm2;
      if (dataObject !== null) {
        $(o.elm2).load(
          base_url + o.url,
          {
            data: dataObject,
          },
          responseCall
        );
      } else {
        $(o.elm2).load(base_url + o.url, responseCall);
      }
    }
  } else {
    if (dataObject !== null) {
      main_el.load(
        base_url + o.url,
        {
          data: dataObject,
        },
        responseCall
      );
    } else {
      main_el.load(base_url + o.url, responseCall);
    }
  }
}

function clickToRemove(o) {
  //o : url, todoAfter (f), target, targetUrl, urlBack
  let timerInterval;
  Swal.fire({
    title: "Anda yakin ingin menghapus data?",
    html:
      "Tindakan ini akan menghapus data dan akan dibatalkan dalam <strong></strong> detik!",
    type: "warning",
    target: o.target,
    timer: 10000,
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Hapus!",
    cancelButtonText: "Batal!",
    onBeforeOpen: () => {
      // Swal.showLoading();
      timerInterval = setInterval(() => {
        Swal.getContent().querySelector("strong").textContent = Math.round(
          Swal.getTimerLeft() / 1000
        );
      }, 1000);
    },
    onClose: () => {
      clearInterval(timerInterval);
    },
  }).then((result) => {
    //console.log(result);
    if (result.value) {
      // true
      goTransmit({
        url: o.url,
        method: "POST",
        todo: function (res) {
          if (
            o.hasOwnProperty("todoAfter") &&
            typeof o.todoAfter === "function"
          ) {
            o.todoAfter(res, o.urlBack, o.targetUrl);
          }
        },
      });
    }
  });
}

function goTransmit(o) {
  //msg, method, urlSend, elmChange, urlBack
  $.ajax({
    type: o.method,
    url: base_url + o.url,
    //data: $('form').serialize(),
    error: responseCall,
    success: function (res) {
      console.log(res);
      if (o.hasOwnProperty("todo") && typeof o.todo === "function") {
        //todo is function
        o.todo(res);
      }
    },
  });
}

//when need to send object, like crop coordinate
function goTransmit2(o) {
  //msg, method, urlSend, elmChange, urlBack
  $.ajax({
    type: o.method,
    url: base_url + o.url,
    data: o.data,
    error: responseCall,
    success: function (res) {
      console.log(res);
      if (o.hasOwnProperty("todo") && typeof o.todo === "function") {
        //todo is function
        o.todo(res);
      }
    },
  });
}

function responseCall(response, status, xhr) {
  // console.log(xhr);
  console.log(elementAccess);
  if (status == "success") {
    // SUCCESSFUL request //
  } else if (status == "error" || status == "timeout") {
    // ERROR and TIMEOUT request //
    if (xhr.status == 403) {
      Toast.fire({
        title: "Anda tidak dapat masuk ke dalam modul",
        target: elementAccess != null ? elementAccess : "body",
        type: "warning",
      });
    }

    if (xhr.status == 400) {
      Toast.fire({
        title: "Server tidak dapat dijangkau",
        target: elementAccess != null ? elementAccess : "body",
        type: "error",
      });
    }

    if (xhr.status == 404) {
      Toast.fire({
        title: "Halaman/Data tidak tersedia",
        target: elementAccess != null ? elementAccess : "body",
        type: "info",
      });
    }

    if (xhr.status == 500) {
      Toast.fire({
        title: "server tidak dapat melayani permintaan",
        target: elementAccess != null ? elementAccess : "body",
        type: "error",
      });
    }
  } else if (status == "notmodified") {
    // NOT MODIFIED request (likely cached) //
  } else if (status == "abort") {
    // ABORTED request //
  }

  //reset elementAccess to null
  elementAccess = null;
}

//custombox
var customBOX = new Custombox.modal({
  content: {
    effect: "slidetogether",
    id: "main-modal-custombox",
    target: "#main-modal",
    positionY: "top",
    positionX: "center",
    delay: 2000,
    onClose: function () {},
  },
  overlay: {
    color: "#111",
    opacity: 0.7,
  },
});

function goPopup(o) {
  //f stand front
  if (o.hasOwnProperty("welm")) {
    $("#main-modal-form-engine").width(o.welm);
    console.log(o.welm);
  } else {
    $("#main-modal-form-engine").width("400px");
  }
  $("#main-modal-form-engine").html(" ");
  goLoad({
    elm: "#main-modal-form-engine",
    url: o.url,
  });
  customBOX.open();
}

const Toast = Swal.mixin({
  toast: true,
  position: "top-end",
  showConfirmButton: false,
  timer: 3000,
});

const ConfirmSwal = Swal.mixin({
  customClass: {
    confirmButton: "btn btn-success",
    cancelButton: "btn btn-danger",
  },
  buttonsStyling: false,
  timer: 5000,
});

function responseModal(o) {
  Swal.fire(o);
  /*{
	  title: 'Do What!',
	  text: 'Do you want to continue',
	  type: 'error',
	  confirmButtonText: 'Cool'
	}*/
}

function goExport(o) {
  window.open(base_url + o.url, "_blank");
}

//smootscrol 1 page
$(function () {
  //http://stackoverflow.com/questions/36341638/unrecognized-expression-ahref
  $('a[href*="#"]:not([href="#"])').click(function () {
    if (
      location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
      if (target.length) {
        $("html,body").animate(
          {
            scrollTop: target.offset().top,
          },
          1000
        );
        return false;
      }
    }
  });
});
